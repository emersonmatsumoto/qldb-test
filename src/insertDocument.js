const {
  createQldbWriter,
  QldbSession,
  QldbWriter,
  Result,
  TransactionExecutor
} = require('amazon-qldb-driver-nodejs')
const { Reader } = require('ion-js')

const { closeQldbSession, createQldbSession } = require('./connectToLedger')
const {
  DRIVERS_LICENSE,
  PERSON,
  VEHICLE,
  VEHICLE_REGISTRATION
} = require('./sampleData')
const {
  DRIVERS_LICENSE_TABLE_NAME,
  PERSON_TABLE_NAME,
  VEHICLE_REGISTRATION_TABLE_NAME,
  VEHICLE_TABLE_NAME
} = require('./qldb/constants')
const { error, log } = require('./qldb/logUtil')
const { getFieldValue, writeValueAsIon } = require('./qldb/util')

/**
 * Insert the given list of documents into a table in a single transaction.
 * @param txn The {@linkcode TransactionExecutor} for lambda execute.
 * @param tableName Name of the table to insert documents into.
 * @param documents List of documents to insert.
 * @returns Promise which fulfills with a {@linkcode Result} object.
 */
const insertDocument = async (txn, tableName, documents) => {
  const statement = `INSERT INTO ${tableName} ?`
  const documentsWriter = createQldbWriter()
  writeValueAsIon(documents, documentsWriter)
  let result = await txn.executeInline(statement, [documentsWriter])
  return result
}

/**
 * Update the PersonId value for DriversLicense records and the PrimaryOwner value for VehicleRegistration records.
 * @param documentIds List of document IDs.
 */
const updatePersonId = documentIds => {
  documentIds.forEach((reader, i) => {
    const documentId = getFieldValue(reader, ['documentId'])
    DRIVERS_LICENSE[i].PersonId = documentId
    VEHICLE_REGISTRATION[i].Owners.PrimaryOwner.PersonId = documentId
  })
}

/**
 * Handle the insertion of documents and updating PersonIds all in a single transaction.
 * @param txn The {@linkcode TransactionExecutor} for lambda execute.
 * @returns Promise which fulfills with void.
 */
const updateAndInsertDocuments = async txn => {
  log("Inserting multiple documents into the 'Person' table...")
  const documentIds = await insertDocument(txn, PERSON_TABLE_NAME, PERSON)

  const listOfDocumentIds = documentIds.getResultList()
  log(
    "Updating PersonIds for 'DriversLicense' and PrimaryOwner for 'VehicleRegistration'..."
  )
  updatePersonId(listOfDocumentIds)

  log('Inserting multiple documents into the remaining tables...')
  await Promise.all([
    insertDocument(txn, DRIVERS_LICENSE_TABLE_NAME, DRIVERS_LICENSE),
    insertDocument(txn, VEHICLE_REGISTRATION_TABLE_NAME, VEHICLE_REGISTRATION),
    insertDocument(txn, VEHICLE_TABLE_NAME, VEHICLE)
  ])
}

/**
 * Insert documents into a table in a QLDB ledger.
 * @returns Promise which fulfills with void.
 */
const main = async () => {
  let session
  try {
    session = await createQldbSession()
    await session.executeLambda(
      async txn => {
        await updateAndInsertDocuments(txn)
      },
      () => log('Retrying due to OCC conflict...')
    )
  } catch (e) {
    error(`Unable to insert documents: ${e}`)
  } finally {
    closeQldbSession(session)
  }
}

if (require.main === module) {
  main()
}
