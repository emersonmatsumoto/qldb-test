const { closeQldbSession, createQldbSession } = require('./connectToLedger')
const {
  DRIVERS_LICENSE_TABLE_NAME,
  PERSON_TABLE_NAME,
  VEHICLE_REGISTRATION_TABLE_NAME,
  VEHICLE_TABLE_NAME
} = require('./qldb/constants')
const { error, log } = require('./qldb/logUtil')

/**
 * Create multiple tables in a single transaction.
 * @param txn The {@linkcode TransactionExecutor} for lambda execute.
 * @param tableName Name of the table to create.
 * @returns Promise which fulfills with the number of changes to the database.
 */
const createTable = async (txn, tableName) => {
  const statement = `CREATE TABLE ${tableName}`
  return await txn.executeInline(statement).then(result => {
    log(`Successfully created table ${tableName}.`)
    return result.getResultList().length
  })
}

/**
 * Create tables in a QLDB ledger.
 * @returns Promise which fulfills with void.
 */
var main = async function() {
  let session
  try {
    session = await createQldbSession()
    await session.executeLambda(
      async txn => {
        Promise.all([
          createTable(txn, VEHICLE_REGISTRATION_TABLE_NAME),
          createTable(txn, VEHICLE_TABLE_NAME),
          createTable(txn, PERSON_TABLE_NAME),
          createTable(txn, DRIVERS_LICENSE_TABLE_NAME)
        ])
      },
      () => log('Retrying due to OCC conflict...')
    )
  } catch (e) {
    error(`Unable to create tables: ${e}`)
  } finally {
    closeQldbSession(session)
  }
}

if (require.main === module) {
  main()
}
