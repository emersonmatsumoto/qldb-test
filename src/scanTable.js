const { decodeUtf8, makePrettyWriter } = require('ion-js')

const { closeQldbSession, createQldbSession } = require('./connectToLedger')
const { log } = require('./qldb/logUtil')

/**
 * Pretty print the Readers in the provided result list.
 * @param resultList The result list containing the Readers to pretty print.
 */
const prettyPrintResultList = resultList => {
  const writer = makePrettyWriter()
  resultList.forEach(reader => {
    writer.writeValues(reader)
  })
  log(decodeUtf8(writer.getBytes()))
}

/**
 * Scan for all the documents in a table.
 * @param txn The {@linkcode TransactionExecutor} for lambda execute.
 * @param tableName The name of the table to operate on.
 * @returns Promise which fulfills with a {@linkcode Result} object.
 */
const scanTableForDocuments = async (txn, tableName) => {
  log(`Scanning ${tableName}...`)
  const query = `SELECT * FROM ${tableName}`
  return await txn.executeInline(query).then(result => {
    return result
  })
}

/**
 * Retrieve the list of table names.
 * @param session The session to retrieve table names from.
 * @returns Promise which fulfills with a list of table names.
 */
const scanTables = async session => {
  return await session.getTableNames()
}

/**
 * Scan for all the documents in a table.
 * @returns Promise which fulfills with void.
 */
const main = async () => {
  let session
  try {
    session = await createQldbSession()
    await scanTables(session).then(
      async listofTables => {
        for (const tableName of listofTables) {
          await session.executeLambda(async txn => {
            const result = await scanTableForDocuments(txn, tableName)
            prettyPrintResultList(result.getResultList())
          })
        }
      },
      () => log('Retrying due to OCC conflict...')
    )
  } catch (e) {
    log(`Error displaying documents: ${e}`)
  } finally {
    closeQldbSession(session)
  }
}

if (require.main === module) {
  main()
}

module.exports = {
  prettyPrintResultList
}