const { isResourceNotFoundException } = require('amazon-qldb-driver-nodejs')
const { QLDB } = require('aws-sdk')

const { setDeletionProtection } = require('./deletionProtection')
const { LEDGER_NAME } = require('./qldb/constants')
const { error, log } = require('./qldb/logUtil')
const { sleep } = require('./qldb/util')

const LEDGER_DELETION_POLL_PERIOD_MS = 20000

/**
 * Send a request to QLDB to delete the specified ledger.
 * @param ledgerName Name of the ledger to be deleted.
 * @param qldbClient The QLDB control plane client to use.
 * @returns Promise which fulfills with void.
 */
const deleteLedger = async (ledgerName, qldbClient) => {
  log(`Attempting to delete the ledger with name: ${ledgerName}`)
  const request = {
    Name: ledgerName
  }
  await qldbClient.deleteLedger(request).promise()
  log('Success.')
}

/**
 * Wait for the ledger to be deleted.
 * @param ledgerName Name of the ledger to be deleted.
 * @param qldbClient The QLDB control plane client to use.
 * @returns Promise which fulfills with void.
 */
const waitForDeleted = async (ledgerName, qldbClient) => {
  log('Waiting for the ledger to be deleted...')
  const request = {
    Name: ledgerName
  }
  while (true) {
    try {
      await qldbClient.describeLedger(request).promise()
      log('The ledger is still being deleted. Please wait...')
      await sleep(LEDGER_DELETION_POLL_PERIOD_MS)
    } catch (e) {
      if (isResourceNotFoundException(e)) {
        log('Success. Ledger is deleted.')
        break
      } else {
        throw e
      }
    }
  }
}

/**
 * Delete a ledger.
 * @returns Promise which fulfills with void.
 */
const main = async () => {
  try {
    const qldbClient = new QLDB({ region: 'us-east-1' })
    await setDeletionProtection(LEDGER_NAME, qldbClient, false)
    await deleteLedger(LEDGER_NAME, qldbClient)
    await waitForDeleted(LEDGER_NAME, qldbClient)
  } catch (e) {
    error(`Unable to delete the ledger: ${e}`)
  }
}

if (require.main === module) {
  main()
}
