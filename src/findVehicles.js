const {
  createQldbWriter,
} = require('amazon-qldb-driver-nodejs')

const { closeQldbSession, createQldbSession } = require('./connectToLedger')
const { PERSON } = require('./sampleData')
const { PERSON_TABLE_NAME } = require('./qldb/constants')
const { error, log } = require('./qldb/logUtil')
const { getDocumentId, writeValueAsIon } = require('./qldb/util')
const { prettyPrintResultList } = require('./scanTable')

/**
 * Query 'Vehicle' and 'VehicleRegistration' tables using a unique document ID in one transaction.
 * @param txn The {@linkcode TransactionExecutor} for lambda execute.
 * @param govId The owner's government ID.
 * @returns Promise which fulfills with void.
 */
const findVehiclesForOwner = async (txn, govId) => {
  const documentId = await getDocumentId(txn, PERSON_TABLE_NAME, 'GovId', govId)
  const query =
    'SELECT Vehicle FROM Vehicle INNER JOIN VehicleRegistration AS r ' +
    'ON Vehicle.VIN = r.VIN WHERE r.Owners.PrimaryOwner.PersonId = ?'
  const qldbWriter = createQldbWriter()
  writeValueAsIon(documentId, qldbWriter)

  await txn.executeInline(query, [qldbWriter]).then(result => {
    const resultList = result.getResultList()
    log(`List of vehicles for owner with GovId: ${govId}`)
    prettyPrintResultList(resultList)
  })
}

/**
 * Find all vehicles registered under a person.
 * @returns Promise which fulfills with void.
 */
const main = async () => {
  let session
  try {
    session = await createQldbSession()
    await session.executeLambda(
      async txn => {
        await findVehiclesForOwner(txn, PERSON[0].GovId)
      },
      () => log('Retrying due to OCC conflict...')
    )
  } catch (e) {
    error(`Error getting vehicles for owner: ${e}`)
  } finally {
    closeQldbSession(session)
  }
}

if (require.main === module) {
  main()
}
