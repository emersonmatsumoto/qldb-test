const { createQldbWriter } = require('amazon-qldb-driver-nodejs')

const { closeQldbSession, createQldbSession } = require('./connectToLedger')
const { VEHICLE_REGISTRATION } = require('./sampleData')
const { VEHICLE_REGISTRATION_TABLE_NAME } = require('./qldb/constants')
const { prettyPrintResultList } = require('./scanTable')
const { error, log } = require('./qldb/logUtil')
const { getDocumentId, writeValueAsIon } = require('./qldb/util')

/**
 * Find previous primary owners for the given VIN in a single transaction.
 * @param txn The {@linkcode TransactionExecutor} for lambda execute.
 * @param vin The VIN to find previous primary owners for.
 * @returns Promise which fulfills with void.
 */
const previousPrimaryOwners = async (txn, vin) => {
  const documentId = await getDocumentId(
    txn,
    VEHICLE_REGISTRATION_TABLE_NAME,
    'VIN',
    vin
  )
  const todaysDate = new Date()
  const threeMonthsAgo = new Date(todaysDate)
  threeMonthsAgo.setMonth(todaysDate.getMonth() - 3)

  const query =
    `SELECT data.Owners.PrimaryOwner, metadata.version FROM history ` +
    `(${VEHICLE_REGISTRATION_TABLE_NAME}, \`${threeMonthsAgo.toISOString()}\`, \`${todaysDate.toISOString()}\`) ` +
    `AS h WHERE h.metadata.id = ?`

  const qldbWriter = createQldbWriter()
  writeValueAsIon(documentId, qldbWriter)

  await txn.executeInline(query, [qldbWriter]).then(result => {
    log(`Querying the 'VehicleRegistration' table's history using VIN: ${vin}.`)
    const resultList = result.getResultList()
    prettyPrintResultList(resultList)
  })
}

/**
 * Query a table's history for a particular set of documents.
 * @returns Promise which fulfills with void.
 */
const main = async () => {
  let session
  try {
    session = await createQldbSession()
    const vin = VEHICLE_REGISTRATION[0].VIN
    await session.executeLambda(
      async txn => {
        await previousPrimaryOwners(txn, vin)
      },
      () => log('Retrying due to OCC conflict...')
    )
  } catch (e) {
    error(`Unable to query history to find previous owners: ${e}`)
  } finally {
    closeQldbSession(session)
  }
}

if (require.main === module) {
  main()
}
