const {
  isResourcePreconditionNotMetException
} = require('amazon-qldb-driver-nodejs')
const { QLDB } = require('aws-sdk')

const { waitForActive } = require('./createLedger')
const { deleteLedger } = require('./deleteLedger')
const { error, log } = require('./qldb/LogUtil')

const LEDGER_NAME = 'deletion-protection-demo'

/**
 * Create a new ledger with the specified name and with deletion protection enabled.
 * @param ledgerName  Name of the ledger to be created.
 * @param qldbClient The QLDB control plane client to use.
 * @returns Promise which fulfills with a CreateLedgerResponse.
 */
const createWithDeletionProtection = async (ledgerName, qldbClient) => {
  log(`Creating a ledger named: ${ledgerName}...`)
  const request = {
    Name: ledgerName,
    PermissionsMode: 'ALLOW_ALL'
  }
  const result = await qldbClient.createLedger(request).promise()
  log(`Success. Ledger state: ${result.State}.`)
  return result
}

/**
 * Update an existing ledger's deletion protection.
 * @param ledgerName Name of the ledger to update.
 * @param qldbClient The QLDB control plane client to use.
 * @param deletionProtection Enables or disables the deletion protection.
 * @returns Promise which fulfills with void.
 */
const setDeletionProtection = async (
  ledgerName,
  qldbClient,
  deletionProtection
) => {
  log(
    `Let's set deletion protection to ${deletionProtection} for the ledger with name ${ledgerName}.`
  )
  const request = {
    Name: ledgerName,
    DeletionProtection: deletionProtection
  }
  const result = await qldbClient.updateLedger(request).promise()
  log(`Success. Ledger updated: ${JSON.stringify(result)}."`)
}

/**
 * Demonstrate the protection of QLDB ledgers against deletion.
 * @returns Promise which fulfills with void.
 */
const main = async () => {
  try {
    const qldbClient = new QLDB()
    await createWithDeletionProtection(LEDGER_NAME, qldbClient)
    await waitForActive(LEDGER_NAME, qldbClient)
    try {
      await deleteLedger(LEDGER_NAME, qldbClient)
    } catch (e) {
      if (isResourcePreconditionNotMetException(e)) {
        log('Ledger protected against deletions!')
      } else {
        throw e
      }
    }
    await setDeletionProtection(LEDGER_NAME, qldbClient, false)
    await deleteLedger(LEDGER_NAME, qldbClient)
  } catch (e) {
    error(`Unable to update or delete the ledger: ${e}`)
  }
}

if (require.main === module) {
  main()
}

module.exports = {
  setDeletionProtection
}
