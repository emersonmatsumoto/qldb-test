const { PooledQldbDriver } = require('amazon-qldb-driver-nodejs')

const { LEDGER_NAME } = require('./qldb/constants')
const { error, log } = require('./qldb/logUtil')

/**
 * Close a QLDB session object.
 * @param session The session to close.
 */
const closeQldbSession = session => {
  if (null != session) {
    session.close()
  }
}

/**
 * Retrieve a QLDB session object.
 * @returns Promise which fufills with a {@linkcode QldbSession} object.
 */
const createQldbSession = async () => {
  const qldbDriver = new PooledQldbDriver(LEDGER_NAME, { region: 'us-east-1' })
  const qldbSession = await qldbDriver.getSession()
  return qldbSession
}

/**
 * Connect to a session for a given ledger using default settings.
 * @returns Promise which fulfills with void.
 */
const main = async () => {
  let session = null
  try {
    session = await createQldbSession()
    log('Listing table names...')
    const tableNames = await session.getTableNames()
    tableNames.forEach(tableName => {
      log(tableName)
    })
  } catch (e) {
    error(`Unable to create session: ${e}`)
  } finally {
    closeQldbSession(session)
  }
}

if (require.main === module) {
  main()
}

module.exports = {
  createQldbSession,
  closeQldbSession
}
