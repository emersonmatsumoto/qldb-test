module.exports = {
  LEDGER_NAME: 'vehicle-registration',
  LEDGER_NAME_WITH_TAGS: 'tags',
  DRIVERS_LICENSE_TABLE_NAME: 'DriversLicense',
  PERSON_TABLE_NAME: 'Person',
  VEHICLE_REGISTRATION_TABLE_NAME: 'VehicleRegistration',
  VEHICLE_TABLE_NAME: 'Vehicle',
  GOV_ID_INDEX_NAME: 'GovId',
  LICENSE_NUMBER_INDEX_NAME: 'LicenseNumber',
  LICENSE_PLATE_NUMBER_INDEX_NAME: 'LicensePlateNumber',
  PERSON_ID_INDEX_NAME: 'PersonId',
  VIN_INDEX_NAME: 'VIN',
  RETRY_LIMIT: 4,
  JOURNAL_EXPORT_S3_BUCKET_NAME_PREFIX: 'qldb-tutorial-journal-export',
  USER_TABLES: 'information_schema.user_tables'
}
