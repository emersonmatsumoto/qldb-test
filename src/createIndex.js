const { closeQldbSession, createQldbSession } = require('./connectToLedger')
const {
  DRIVERS_LICENSE_TABLE_NAME,
  GOV_ID_INDEX_NAME,
  LICENSE_NUMBER_INDEX_NAME,
  LICENSE_PLATE_NUMBER_INDEX_NAME,
  PERSON_ID_INDEX_NAME,
  PERSON_TABLE_NAME,
  VEHICLE_REGISTRATION_TABLE_NAME,
  VEHICLE_TABLE_NAME,
  VIN_INDEX_NAME
} = require('./qldb/constants')
const { error, log } = require('./qldb/logUtil')

/**
 * Create an index for a particular table.
 * @param txn The {@linkcode TransactionExecutor} for lambda execute.
 * @param tableName Name of the table to add indexes for.
 * @param indexAttribute Index to create on a single attribute.
 * @returns Promise which fulfills with the number of changes to the database.
 */
createIndex = async (txn, tableName, indexAttribute) => {
  const statement = `CREATE INDEX on ${tableName} (${indexAttribute})`
  return await txn.executeInline(statement).then(result => {
    log(`Successfully created index ${indexAttribute} on table ${tableName}.`)
    return result.getResultList().length
  })
}

/**
 * Create indexes on tables in a particular ledger.
 * @returns Promise which fulfills with void.
 */
const main = async () => {
  let session
  try {
    session = await createQldbSession()
    await session.executeLambda(
      async txn => {
        Promise.all([
          createIndex(txn, PERSON_TABLE_NAME, GOV_ID_INDEX_NAME),
          createIndex(txn, VEHICLE_TABLE_NAME, VIN_INDEX_NAME),
          createIndex(txn, VEHICLE_REGISTRATION_TABLE_NAME, VIN_INDEX_NAME),
          createIndex(
            txn,
            VEHICLE_REGISTRATION_TABLE_NAME,
            LICENSE_PLATE_NUMBER_INDEX_NAME
          ),
          createIndex(txn, DRIVERS_LICENSE_TABLE_NAME, PERSON_ID_INDEX_NAME),
          createIndex(
            txn,
            DRIVERS_LICENSE_TABLE_NAME,
            LICENSE_NUMBER_INDEX_NAME
          )
        ])
      },
      () => log('Retrying due to OCC conflict...')
    )
  } catch (e) {
    error(`Unable to create indexes: ${e}`)
  } finally {
    closeQldbSession(session)
  }
}

if (require.main === module) {
  main()
}
