const { createQldbWriter } = require("amazon-qldb-driver-nodejs")

const { closeQldbSession, createQldbSession } = require("./connectToLedger")
const { PERSON, VEHICLE } = require("./sampleData")
const { PERSON_TABLE_NAME } = require("./qldb/constants")
const { error, log } = require("./qldb/logUtil")
const { getDocumentId, getFieldValue, writeValueAsIon } = require("./qldb/util")

/**
 * Query a driver's information using the given ID.
 * @param txn The {@linkcode TransactionExecutor} for lambda execute.
 * @param documentId The unique ID of a document in the Person table.
 * @returns Promise which fulfills with a Reader containing the person.
 */
const findPersonFromDocumentId = async (txn, documentId) => {
    const query = "SELECT p.* FROM Person AS p BY pid WHERE pid = ?";

    const qldbWriter = createQldbWriter();
    writeValueAsIon(documentId, qldbWriter);

    let personId;
    await txn.executeInline(query, [qldbWriter]).then((result) => {
        const resultList = result.getResultList();
        if (resultList.length === 0) {
            throw new Error(`Unable to find person with ID: ${documentId}.`);
        }
        personId = resultList[0];
    });
    return personId;
}

/**
 * Find the primary owner for the given VIN.
 * @param txn The {@linkcode TransactionExecutor} for lambda execute.
 * @param vin The VIN to find primary owner for.
 * @returns Promise which fulfills with a Reader containing the primary owner.
 */
const findPrimaryOwnerForVehicle = async (txn, vin) => {
    log(`Finding primary owner for vehicle with VIN: ${vin}`);
    const query = "SELECT Owners.PrimaryOwner.PersonId FROM VehicleRegistration AS v WHERE v.VIN = ?";
    const vinWriter = createQldbWriter();
    writeValueAsIon(vin, vinWriter);

    let documentId;
    await txn.executeInline(query, [vinWriter]).then((result) => {
        const resultList = result.getResultList();
        if (resultList.length === 0) {
            throw new Error(`Unable to retrieve document ID using ${vin}.`);
        }
        documentId = getFieldValue(resultList[0], ["PersonId"]);
    });
    return findPersonFromDocumentId(txn, documentId);
}

/**
 * Update the primary owner for a vehicle using the given VIN.
 * @param txn The {@linkcode TransactionExecutor} for lambda execute.
 * @param vin The VIN for the vehicle to operate on.
 * @param documentId New PersonId for the primary owner.
 * @returns Promise which fulfills with void.
 */
const updateVehicleRegistration = async (txn, vin, documentId) => {
    const statement = "UPDATE VehicleRegistration AS r SET r.Owners.PrimaryOwner.PersonId = ? WHERE r.VIN = ?";

    const personIdWriter = createQldbWriter();
    writeValueAsIon(documentId, personIdWriter);

    const vinWriter = createQldbWriter();
    writeValueAsIon(vin, vinWriter);

    log(`Updating the primary owner for vehicle with VIN: ${vin}...`);
    await txn.executeInline(statement, [personIdWriter, vinWriter]).then((result) => {
        const resultList = result.getResultList();
        if (resultList.length === 0) {
            throw new Error("Unable to transfer vehicle, could not find registration.");
        }
        log(`Successfully transferred vehicle with VIN ${vin} to new owner.`);
    });
}

/**
 * Validate the current owner of the given vehicle and transfer its ownership to a new owner in a single transaction.
 * @param txn The {@linkcode TransactionExecutor} for lambda execute.
 * @param vin The VIN of the vehicle to transfer ownership of.
 * @param currentOwner The GovId of the current owner of the vehicle.
 * @param newOwner The GovId of the new owner of the vehicle.
 */
const validateAndUpdateRegistration = async (
    txn,
    vin,
    currentOwner,
    newOwner
) => {
    const primaryOwner = await findPrimaryOwnerForVehicle(txn, vin);
    if (getFieldValue(primaryOwner, ["GovId"]) !== currentOwner) {
        log("Incorrect primary owner identified for vehicle, unable to transfer.");
    }
    else {
        const documentId = await getDocumentId(txn, PERSON_TABLE_NAME, "GovId", newOwner);
        await updateVehicleRegistration(txn, vin, documentId);
        log("Successfully transferred vehicle ownership!");
    }
}

/**
 * Find primary owner for a particular vehicle's VIN.
 * Transfer to another primary owner for a particular vehicle's VIN.
 * @returns Promise which fulfills with void.
 */
const main = async () => {
    let session;
    try {
        session = await createQldbSession();

        const vin = VEHICLE[0].VIN;
        const previousOwnerGovId = PERSON[0].GovId;
        const newPrimaryOwnerGovId = PERSON[1].GovId;

        await session.executeLambda(async (txn) => {
            await validateAndUpdateRegistration(txn, vin, previousOwnerGovId,  newPrimaryOwnerGovId);
        }, () => log("Retrying due to OCC conflict..."));
    } catch (e) {
        error(`Unable to connect and run queries: ${e}`);
    } finally {
        closeQldbSession(session);
    }
}

if (require.main === module) {
    main();
}