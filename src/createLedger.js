const { QLDB } = require('aws-sdk')

const { LEDGER_NAME } = require('./qldb/constants')
const { error, log } = require('./qldb/logUtil')
const { sleep } = require('./qldb/util')

const LEDGER_CREATION_POLL_PERIOD_MS = 10000
const ACTIVE_STATE = 'ACTIVE'

/**
 * Create a new ledger with the specified name.
 * @param ledgerName Name of the ledger to be created.
 * @param qldbClient The QLDB control plane client to use.
 * @returns Promise which fulfills with a CreateLedgerResponse.
 */
const createLedger = async(ledgerName, qldbClient) => {
  log(`Creating a ledger named: ${ledgerName}...`)
  const request = {
    Name: ledgerName,
    PermissionsMode: 'ALLOW_ALL'
  }
  const result = await qldbClient.createLedger(request).promise()
  log(`Success. Ledger state: ${result.State}.`)
  return result
}

/**
 * Wait for the newly created ledger to become active.
 * @param ledgerName Name of the ledger to be checked on.
 * @param qldbClient The QLDB control plane client to use.
 * @returns Promise which fulfills with a DescribeLedgerResponse.
 */
const waitForActive = async (ledgerName, qldbClient) => {
  log(`Waiting for ledger ${ledgerName} to become active...`)
  const request = {
    Name: ledgerName
  }
  while (true) {
    const result = await qldbClient.describeLedger(request).promise()
    if (result.State === ACTIVE_STATE) {
      log('Success. Ledger is active and ready to be used.')
      return result
    }
    log('The ledger is still creating. Please wait...')
    await sleep(LEDGER_CREATION_POLL_PERIOD_MS)
  }
}

/**
 * Create a ledger and wait for it to be active.
 * @returns Promise which fulfills with void.
 */
const main = async () => {
  try {
    const qldbClient = new QLDB({
      region: 'us-east-1'
    })
    await createLedger(LEDGER_NAME, qldbClient)
    await waitForActive(LEDGER_NAME, qldbClient)
  } catch (e) {
    error(`Unable to create the ledger: ${e}`)
  }
}

main().then(() => console.log('finish'))